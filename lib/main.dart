import 'package:flutter/material.dart';
import 'package:test_ipcam_stream/frame_stream.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter IPCAM',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'IPCAM'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.redAccent,
          title: Text(widget.title),
        ),
        body: ListView(
          children: [
            Container(
                height: 400,

                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: [
                    Container(

                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: WebView(
                          initialUrl:
                          'http://192.168.1.133:41956/videostream.cgi?user=admin&pwd=88888888',
                        ),
                      ),
                    ),
                    Align(

                      child: Text(
                        "เขียนแบบง่ายๆ",
                        style:
                        TextStyle(color: Colors.blueGrey, fontSize: 20),
                      ),
                    )
                  ],
                )),
            Container(
              height: 300,
              margin: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: FrameStream(
                      error: (context, error, stack) {
                        print(error);
                        print(stack);
                        return Text(error.toString(),
                            style: TextStyle(color: Colors.red));
                      },
                      stream:
                      'http://192.168.1.133:41956/videostream.cgi?user=admin&pwd=88888888',
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "เขียนดึงเอง",
                      style: TextStyle(color: Colors.blueGrey, fontSize: 20),
                    ),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
